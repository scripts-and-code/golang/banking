package domain

type CustomerRepositoryStub struct {
	customers []Customer
}

func (s CustomerRepositoryStub) FindAll() ([]Customer, error) {
	return s.customers, nil
}

func NewCustomerRepositoryStub() CustomerRepositoryStub {
	customers := []Customer{
		{Id: "1000", Name: "Yan", City: "Novgorod", Zipcode: "11070", DateOfBirth: "1991-08-08", Status: "1"},
		{Id: "1001", Name: "Nastia", City: "Beograd", Zipcode: "11050", DateOfBirth: "1992-05-15", Status: "1"},
	}
	return CustomerRepositoryStub{customers: customers}

}
